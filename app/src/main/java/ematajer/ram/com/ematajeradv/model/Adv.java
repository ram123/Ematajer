package ematajer.ram.com.ematajeradv.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ram on 7/9/17.
 */

public class Adv implements Parcelable{
    private int adId;
    private String adType;
    private String title;
    private String adDetail;
    private double longitude;
    private double latitude;
    private int image;

    public Adv(int adId, String adType, String title,String adDetail, double latitude, double longitude, int image) {
        this.adId = adId;
        this.adType = adType;
        this.title = title;
        this.adDetail = adDetail;
        this.longitude = longitude;
        this.latitude = latitude;
        this.image = image;
    }

    protected Adv(Parcel in) {
        adId = in.readInt();
        adType = in.readString();
        title = in.readString();
        adDetail = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        image = in.readInt();
    }

    public static final Creator<Adv> CREATOR = new Creator<Adv>() {
        @Override
        public Adv createFromParcel(Parcel in) {
            return new Adv(in);
        }

        @Override
        public Adv[] newArray(int size) {
            return new Adv[size];
        }
    };

    public int getAdId() {
        return adId;
    }

    public String getAdType() {
        return adType;
    }

    public String getTitle() {
        return title;
    }

    public String getAdDetail() {
        return adDetail;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public int getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Adv{" +
                "adId=" + adId +
                ", adType='" + adType + '\'' +
                ", title='" + title + '\'' +
                ", adDetail='" + adDetail + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", image=" + image +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(adId);
        parcel.writeString(adType);
        parcel.writeString(title);
        parcel.writeString(adDetail);
        parcel.writeDouble(longitude);
        parcel.writeDouble(latitude);
        parcel.writeInt(image);
    }
}
