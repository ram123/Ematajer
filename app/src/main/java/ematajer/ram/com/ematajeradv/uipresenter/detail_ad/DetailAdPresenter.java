package ematajer.ram.com.ematajeradv.uipresenter.detail_ad;

/**
 * Created by ram on 7/9/17.
 */

public interface DetailAdPresenter {

    void addView(DetailAdView detailAdView);

    void requestDetail(int adId,String type);
}
