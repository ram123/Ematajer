package ematajer.ram.com.ematajeradv.uipresenter.clothes_ad;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ematajer.ram.com.ematajeradv.R;
import ematajer.ram.com.ematajeradv.model.Adv;
import ematajer.ram.com.ematajeradv.uipresenter.detail_ad.DetailAdActivity;
import ematajer.ram.com.ematajeradv.uipresenter.direction.DirectionMapActivity;
import ematajer.ram.com.ematajeradv.utils.DistanceCalculator;
import ematajer.ram.com.ematajeradv.utils.EventBus;
import ematajer.ram.com.ematajeradv.utils.Events;
import ematajer.ram.com.ematajeradv.utils.MetaData;

public class FragmentClothes extends Fragment {

    @Bind(R.id.ad_image)
    ImageView adImageView;
    @Bind(R.id.ad_title)
    TextView adTitle;
    @Bind(R.id.ad_intro)
    TextView adDetail;

    private Adv adv;
    private List<Adv> clothesAds;

    public FragmentClothes() {
        // Required empty public constructor
    }

    public static FragmentClothes newInstance() {
        return new FragmentClothes();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ad_restaurant_clothes, container, false);
        ButterKnife.bind(this, root);
        EventBus.register(this);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clothesAds = MetaData.getClotheAds();
    }

    @OnClick(R.id.btn_show_detail)
    public void onBtnShowDetailClicked() {
        Intent i = new Intent(getContext(), DetailAdActivity.class);
        i.putExtra(MetaData.KEY_AD, adv);
        startActivity(i);
    }

    @OnClick(R.id.btn_show_direction)
    public void onBtnShowDirectionClicked() {
        Intent i = new Intent(getContext(), DirectionMapActivity.class);
        i.putExtra(MetaData.KEY_AD, adv);
        startActivity(i);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.unregister(this);
    }

    @Subscribe
    public void onLocationReceived(Events.LocationEvent locationEvent) {
        double shortdistance = 0;
        for (int i = 0; i < clothesAds.size(); i++) {
            adv = clothesAds.get(i);

            double actualDistance = DistanceCalculator.getDistance(locationEvent.getLatitude(), locationEvent.getLongitue(),
                    adv.getLatitude(), adv.getLongitude(), "K");

            //assume the distance of the adv of index 0 is shortest distance
            //now for the rest of the Adv from restaurants ads we compare this
            // assumed distance with the actual distance to get the actual Shortest distance
            if (i == 0) {
                shortdistance = actualDistance;
            } else {
                if (actualDistance < shortdistance) {
                    shortdistance = actualDistance;
                }
            }
            Log.i("Distance", String.valueOf(actualDistance));
        }
        Log.i("ShortestDistance", String.valueOf(shortdistance));

        adImageView.setImageResource(adv.getImage());
        adTitle.setText(adv.getTitle());
        adDetail.setText(getResources().getString(R.string.intro_text, adv.getAdDetail().substring(1, 200)));
    }
}
