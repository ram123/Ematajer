package ematajer.ram.com.ematajeradv.uipresenter.detail_ad;

import ematajer.ram.com.ematajeradv.utils.MetaData;

/**
 * Created by ram on 7/9/17.
 */

public class DetailAdPresenterImpl implements DetailAdPresenter {
    private DetailAdView adView;

    @Override
    public void addView(DetailAdView detailAdView) {
        adView = detailAdView;
    }

    @Override
    public void requestDetail(int adId, String adType) {
        //// TODO: 7/9/17
        adView.showProgress();

        adView.onDetailDataReceived(MetaData.getAd(adId, adType));

        adView.hideProgress();
    }
}
