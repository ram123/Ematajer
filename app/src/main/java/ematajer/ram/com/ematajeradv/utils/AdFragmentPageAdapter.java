package ematajer.ram.com.ematajeradv.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ematajer.ram.com.ematajeradv.uipresenter.clothes_ad.FragmentClothes;
import ematajer.ram.com.ematajeradv.uipresenter.resturants_ad.FragmentRestaurant;

/**
 * Created by ram on 7/9/17.
 */

public class AdFragmentPageAdapter extends FragmentStatePagerAdapter {
    String[] tabTitle = new String[]{"RESTAURANTS", "CLOTHES"};

    public AdFragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentRestaurant.newInstance();
            case 1:
                return FragmentClothes.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitle[position];
    }
}
