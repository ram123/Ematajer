package ematajer.ram.com.ematajeradv.uipresenter.direction;

/**
 * Created by ram on 7/10/17.
 */

public interface DirectionPresenter {

    void onAddDirectionView(DirectionAdView directionAdView);

    void onGetPolyLineOptions(String url);

}
