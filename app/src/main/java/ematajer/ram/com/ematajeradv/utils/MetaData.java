package ematajer.ram.com.ematajeradv.utils;

import java.util.ArrayList;
import java.util.List;

import ematajer.ram.com.ematajeradv.R;
import ematajer.ram.com.ematajeradv.model.Adv;

/**
 * Created by ram on 7/9/17.
 */

public class MetaData {

    private static String restaurantAdDetail = "As the name suggests, it serves every part of the pig you can think of, around the clock. \n\nFavourite haunt of hungry late-night drinkers, there's something fortifying in the old-style brasserie décor as well as the hearty dishes. Among imitation leather banquettes, Belle Epoque lamps and paintings, white tablecloths and waiters in penguin suits, kitsch little details show a sense of humour – where else can you push a gilt pig’s foot to get to the toilets, or dunk a pink meringue piglet in your coffee?\n" +
            "\nThe menu covers traditional brasserie cooking – seafood, onion soup, steak tartare and crêpes flambées – but its raison d’être is the fat and flesh of the pig, the star of the show. Stuffed trotters, head cassoulet, smoked belly, tail, ear and brawn... hardly a light supper, but a genuine thrill for fans of eating 'nose to tail'.\n";

    private static String clotheAdDetail = "For the latest in womens fashion straight from the catwalk, Missguided is where it's at! We’ve got all the womens clothing ranges to make sure you’re maximising your attitude whilst taking your style game to new heights. Revamp your wardrobe with fresh fabrics straight from the new season – whether it’s dropping dollar on some artisan embroidery for a subtle feminine kick, adding a techy touch for a metallic, space-age vibe or keeping it grunge in ripped detailing and neutral tones. \n\nHead into the party season and demand attention in strictly sequins or make the dancefloor yours in glitzy embellishment. Afterall, when you’re daring and bold, anything goes. From casual, off-duty looks to maximum-mileage dresses and outfits, we bet your bottom dollar you’ll be splashing out on more than just one. Get browsin’, get clickin’ and get lookin’ lit af.";
    public static String TYPE_RESTAURANT = "restaurant";
    public static String TYPE_CLOTHE = "clothes";

    //used for args key
    public static String KEY_AD = "adv";

    public static List<Adv> getRestaurantAds() {
        List<Adv> ads = new ArrayList<>();
        ads.add(new Adv(1, TYPE_RESTAURANT, "Maijula party Palace", restaurantAdDetail, 27.651371,85.3277125, R.drawable.restaurant_1));
        ads.add(new Adv(2, TYPE_RESTAURANT, "De Khaana Cafe", restaurantAdDetail, 27.6502306,85.3298798, R.drawable.restaurant_2));
        ads.add(new Adv(3, TYPE_RESTAURANT, "Dhapakhel Cafe", restaurantAdDetail, 27.637665,85.3216349, R.drawable.restaurant_1));
        ads.add(new Adv(4, TYPE_RESTAURANT, "Tewa", restaurantAdDetail, 27.639012,85.3258318, R.drawable.restaurant_2));

        return ads;
    }

    public static List<Adv> getClotheAds() {
        List<Adv> ads = new ArrayList<>();
        ads.add(new Adv(1, TYPE_CLOTHE, "East North", clotheAdDetail, 27.651371,85.3277125, R.drawable.cloth_store_1));
        ads.add(new Adv(2, TYPE_CLOTHE, "East South", clotheAdDetail, 27.637367, 85.3278748, R.drawable.cloth_store_2));
        ads.add(new Adv(3, TYPE_CLOTHE, "Dhapakhel Cloths", clotheAdDetail, 27.637665,85.3216349, R.drawable.cloth_store_1));
        ads.add(new Adv(4, TYPE_CLOTHE, "Tewa clothes store", clotheAdDetail, 27.639012,85.3258318, R.drawable.cloth_store_2));

        return ads;
    }


    public static Adv getAd(int id, String type) {
        /**
         * I could have done this by passing the data between fragment and activity directly
         * but this might be the scenario of getting detail by making api request
         * from the remote server so I am calling all the ads and after that getting detail by id and adType
         */

        List<Adv> ads = new ArrayList<>();
        ads.addAll(getRestaurantAds());
        ads.addAll(getClotheAds());

        for (Adv ad : ads) {

            if (ad.getAdId() == id
                    && ad.getAdType().equals(type)) {
                return ad;
            }
        }

        return null;
    }
}
