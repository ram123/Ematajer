package ematajer.ram.com.ematajeradv.uipresenter.detail_ad;

import ematajer.ram.com.ematajeradv.model.Adv;

/**
 * Created by ram on 7/9/17.
 */

public interface DetailAdView {
    void showProgress();

    void hideProgress();

    void onDetailDataReceived(Adv adv);
}
