package ematajer.ram.com.ematajeradv.utils;

/**
 * Created by ram on 7/10/17.
 */

public class Events {

    public static class LocationEvent {

        private double latitude;
        private double longitue;

        public LocationEvent(double latitude,double longitue) {
            this.latitude = latitude;
            this.longitue = longitue;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitue() {
            return longitue;
        }
    }
}
