package ematajer.ram.com.ematajeradv.uipresenter.detail_ad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ematajer.ram.com.ematajeradv.R;
import ematajer.ram.com.ematajeradv.model.Adv;
import ematajer.ram.com.ematajeradv.uipresenter.direction.DirectionMapActivity;
import ematajer.ram.com.ematajeradv.utils.MetaData;

/**
 * Created by ram on 7/9/17.
 */

public class DetailAdActivity extends AppCompatActivity implements DetailAdView {

    @Bind(R.id.ad_image)
    ImageView adImageView;
    @Bind(R.id.ad_title)
    TextView adTitle;
    @Bind(R.id.ad_intro)
    TextView adDetail;
    @Bind(R.id.btn_show_detail)
    Button showDetail;

    private ProgressDialog dialog;
    private DetailAdPresenter presenter;
    private Adv adv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        ButterKnife.bind(this);

        adv = getIntent().getParcelableExtra(MetaData.KEY_AD);


        dialog = new ProgressDialog(this);
        dialog.setMessage("please wait...");
        dialog.setCancelable(true);

        /**
         * We could have directly shown the adv obtained from the intent instead of calling requestDetail
         * but I thought there might be more specific if we are working with the api request
         */
        presenter = new DetailAdPresenterImpl();
        presenter.addView(this);
        presenter.requestDetail(adv.getAdId(), adv.getAdType());



        //hide the show detail btn
        showDetail.setVisibility(View.GONE);

    }

    @Override
    public void showProgress() {
        dialog.onStart();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onDetailDataReceived(Adv adv) {
        adImageView.setImageResource(adv.getImage());
        adTitle.setText(adv.getTitle());
        adDetail.setText(adv.getAdDetail());
    }

    @OnClick(R.id.btn_show_direction)
    public void onBtnShowDirectionClicked() {
        Intent i = new Intent(this, DirectionMapActivity.class);
        i.putExtra(MetaData.KEY_AD, adv);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
